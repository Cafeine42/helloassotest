<?php

declare(strict_types=1);

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class HomeController extends AbstractController
{
    public function __construct(
        private readonly HttpClientInterface $httpClient,
        #[Autowire(env: 'OPEN_DATA_URL')] private readonly string $openDataUrl,
    ) {
    }

    #[Route('/')]
    public function home(): Response
    {
        $request = $this->httpClient->request('GET', $this->openDataUrl);

        // Extract json
        $content = $request->toArray();

        $data = array_map(function (array $item) {
            return [
                'bm_heure' => $item['fields']['bm_heure'],
                'bm_prevision' => $item['fields']['bm_prevision'],
            ];
        }, $content['records']);

        return $this->render('home/index.html.twig', [
            'data' => $data,
        ]);
    }
}
